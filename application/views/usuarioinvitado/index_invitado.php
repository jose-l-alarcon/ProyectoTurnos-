<!DOCTYPE html>
  <html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Inicio</title>

      <script src="<?= base_url()?>plantilla/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="<?= base_url()?>plantilla/https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="<?= base_url()?>plantilla/js/vendor/bootstrap.min.js"></script>      
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="<?= base_url()?>plantilla/js/easing.min.js"></script>      
      <script src="<?= base_url()?>plantilla/js/hoverIntent.js"></script>
      <script src="<?= base_url()?>plantilla/js/superfish.min.js"></script> 
      <script src="<?= base_url()?>plantilla/js/jquery.ajaxchimp.min.js"></script>
      <script src="<?= base_url()?>plantilla/js/jquery.magnific-popup.min.js"></script> 
      <script src="<?= base_url()?>plantilla/js/jquery-ui.js"></script>     
      <script src="<?= base_url()?>plantilla/js/owl.carousel.min.js"></script>            
      <script src="<?= base_url()?>plantilla/js/jquery.nice-select.min.js"></script>              
      <script src="<?= base_url()?>plantilla/js/mail-script.js"></script> 
      <script src="<?= base_url()?>plantilla/js/main.js"></script>  
     
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
      <!--
      CSS
      ============================================= -->
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/linearicons3.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/font-awesome.min.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/bootstrap.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/magnific-popup.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/nice-select.css">              
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/animate.min.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/jquery-ui.css">      
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/owl.carousel.css">
      <link rel="stylesheet" href="<?= base_url()?>plantilla/css/main6.css">

    </head>
    <body>  
        <header id="header" id="home">
          <div class="header-top">
            <div class="container">
              <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6 col-4 header-top-left no-padding">
                     <h6 class="text-uppercase">Dra. Teresita Santillán</h6>
                     <h6 class="text-uppercase">Dermatóloga</h6>
                </div>
                <div class="col-lg-6 col-sm-6 col-8 header-top-right no-padding">
                <a class="btns" href="tel:+953 012 3654 896">Tel: 0379 446-4373 </a> 
                  <a class="icons" href="tel:+953 012 3654 896">
                    <span class="lnr lnr-phone-handset"></span>
                  </a>
                  <a class="icons" href="mailto:support@colorlib.com">
                    <span class="lnr lnr-envelope"></span>
                  </a>    
                </div>
              </div>                  
            </div>
        </div>
          <div class="container main-menu">
            <div class="row align-items-center justify-content-between d-flex">
              <nav id="nav-menu-container">
                <ul class="nav-menu">
                  <li class="menu-active"><a href="index.html">Inicio</a></li>
                  <li><a href="services.html">Obras Sociales y Prepagas</a></li>
                  <li class="menu-has-children"><a href="">Tratamientos</a>
                    <ul>
                      <li><a href="blog-home.html">Blog Home</a></li>
                      <li><a href="blog-single.html">Blog Single</a></li>
                    <li class="menu-has-children"><a href="">Level 2</a>
                      <ul>
                        <li><a href="#">Item One</a></li>
                        <li><a href="#">Item Two</a></li>
                      </ul>
                    </li>                       
                    </ul>
                   <li><a href="services.html">Institucional</a></li>
                  </li>                           
                  <li><a href="redirect(/Welcome.php/usuarioinvitado/contacto_invitado.php">Contacto</a></li>
                </ul> 
              </nav><!-- #nav-menu-container -->
              <div class="menu-social-icons">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-behance"></i></a>
          </div>          
            </div>
          </div>
        </header><!-- #header -->

      <!-- start banner Area -->
      <section class="banner-area relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row fullscreen d-flex justify-content-center align-items-center">
            <div class="banner-content col-lg-9 col-md-12 justify-content-center">
              <h6 class="text-uppercase">Conoce nuestros tratamientos </h6>
              <h1>
                Dermatología & Estética      
              </h1>
              <p class="text-white mx-auto">
                Somos una empresa dedicada a la prestación de servicios relacionados con la medicina estética y la criocirugía en general. Lo invitamos a que disfrute de todo lo que tenemos para ofrecerle. Bienvenido.
              </p>
              <a href="#" class="primary-btn header-btn text-uppercase mt-10">Turnos online</a>
            </div>                      
          </div>
        </div>
      </section>
      <!-- End banner Area -->

      <!-- Start open-hour Area -->
      <section class="open-hour-area">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-12 open-hour-wrap">
              <h1>Horarios de atención</h1> <br/>
              <a class="open-btn" href="#"> <span class="circle"></span>Turnos Online</a>
              <p>
                + rápido + fácil + cómodo 
              </p>
              <div class="date-list d-flex flex-row justify-content-center">
                <ul class="colm-1">
                  <li>Lunes - Miercoles</li>
                  <li>Martes - jueves - Viernes</li>
                  <li>Sabados</li>
                </ul>
                <ul class="colm-2">
                  <li><span>:</span>   10:00am a 12:00pm</li>
                  <li><span>:</span>   06:00pm a 08:00pm</li>
                  <li><span>:</span>   10:00am a 12:00am</li>
                </ul>               
              </div>
            </div>
          </div>
        </div>  
      </section>
      <!-- End open-hour Area -->
      
      
      <!-- Start service Area -->
      <section class="service-area section-gap">
        <div class="container">
          <div class="row d-flex justify-content-center">
            <div class="col-md-12 pb-40 header-text text-center">
              <h1 class="pb-10">Nuestros tratamientos</h1>
            </div>
          </div>              
          <div class="row">
            <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/lunares.jpg" alt="">
                <a href="#"><h4>Lunares</h4></a>
                <p>
                  Los lunares tienen que ser controlados por el Dermatólogo. Esto posibilita que si tenés algún lunar maligno, el profesional lo va a detectar tempranamente reduciendo el riesgo que presenta para tu salud.
                </p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/peeling.jpg" alt="">
                <a href="#"><h4>Peeling</h4></a>
                <p>
                  Si no te convence el aspecto estético de tu piel un peeling químico es una alternativa interesante para conseguir el cambio deseado. 
                </p>
              </div>
            </div>
            <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/rejuvenecimiento.jpg" alt="">
                <a href="#"><h4>Rejuvenecimiento</h4></a>
                <p>
                 Distintos factores intrínsecos y extrínsecos (edad, sol, cigarrillo, desórdenes patológicos) hacen que nuestra piel tenga un aspecto envejecido o se presente con imperfecciones no deseadas
                </p>
              </div>
            </div>  
            <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/uña.jpg" alt="">
                <a href="#"><h4>Onicomicosis</h4></a>
                <p>
                 La onicomicosis o la aparición de hongos en las uñas es una dolencia infecciosa muy frecuente que preocupa a quien lo padece porque resulta inestético. Se calcula que el 8% de la población padece este problema.
                </p>
              </div>
            </div>  
               <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/meso.jpg" alt="">
                <a href="#"><h4>Mesoterapia</h4></a>
                <p>
                La mesoterapia capilar es un tratamiento que consiste en la aplicación de nutrientes y medicación específica 
                 en el cuero cabelludo, para que su acción llegue de forma directa al folículo piloso.
               </p>
              </div>
            </div>     
             <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/manchas.jpg" alt="">
                <a href="#"><h4>Manchas</h4></a>
                <p>
                Es muy frecuente la aparición de manchas de distintios apecto, color , tamaño y forma en el rostro tanto en hombres como de mujeres. Aparecen en personas que tienen una piel predispuesta a mancharse.
               </p>
              </div>
            </div> 
              <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/Rosacea.jpg" alt="">
                <a href="#"><h4>Rosácea</h4></a>
                <p>
                La rosácea afecta de manera más común a las mujeres de mediana edad con piel clara. Puede ser confundida con el acné o con otras afecciones de la piel.
               </p>
              </div>
            </div>    
             <div class="col-lg-3 col-md-6">
              <div class="single-service">
                <img class="img-fluid" src="<?=base_url()?>plantilla/img/acne.jpg" alt="">
                <a href="#"><h4>Exceso de Grasitud en la piel</h4></a>
                <p>
                 Las pieles grasas se caracterizan por el aumento de la seborrea(grasitud) debido a una mayor secreción de un maaterial graso llamado sebo sobre la superficie de la piel.
               </p>
              </div>
            </div>                               
          </div>
        </div>  

      </section>
      <!-- End service Area -->

      <!-- Start home-about Area -->
      <section class="home-about-area section-gap relative">      
        <div class="container">
          <div class="row align-items-center justify-content-end">
            <div class="col-lg-6 no-padding home-about-right">
              <h1 class="text-white">
                Conózcanos<br>
              </h1>
              <p>
               Nuestro consultorio se destaca en la localidad de Corrientes, debido a la realización de obras sociales y nuestra opción de medicina prepaga. Estamos a su disposición.
              </p>
             <div class="row no-gutters">
                <div class="single-services col">
                  <span class="lnr lnr-diamond"></span>
                  <a>
                    <h4 class="text-white">Dirección:</h4>
                  </a>
                  <p style=color:#67bc00;>
                       General Belgrano 1255 </br>
                       Corrientes CORRIENTES 3400 
                  </p>
                </div>
                <div class="single-services col">
                  <span class="lnr lnr-phone"></span>
                    <h4 class="text-white"> Teléfono: </h4>
                  <p style=color:#67bc00;>
                    (0379)446 4373
                  </p> 
                </div>                
              </div>
            </div>
          </div>
        </div>  
      </section>
      <!-- End home-about Area -->  

      <!-- start footer Area -->    
      <footer class="footer-area section-gap">
        <div class="container">
           <div class="row footer-bottom d-flex justify-content-between">
            <p class="col-lg-8 col-sm-12 footer-text m-0" ><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heaart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">jose Alarcon </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-sm-12 footer-social">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-twitter"></i></a>
           <!--    <a href="#"><i class="fa fa-dribbble"></i></a>
              <a href="#"><i class="fa fa-behance"></i></a> -->
            </div>
          </div>
        </div>
      </footer>
      <!-- End footer Area -->

     
    </body>
  </html>